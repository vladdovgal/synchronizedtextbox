﻿namespace WinFormsEx1
{
    partial class ChildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxChildForm = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxChildForm
            // 
            this.textBoxChildForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxChildForm.Location = new System.Drawing.Point(48, 28);
            this.textBoxChildForm.Name = "textBoxChildForm";
            this.textBoxChildForm.Size = new System.Drawing.Size(337, 34);
            this.textBoxChildForm.TabIndex = 0;
            this.textBoxChildForm.TextChanged += new System.EventHandler(this.textBoxChildForm_TextChanged);
            // 
            // ChildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 99);
            this.Controls.Add(this.textBoxChildForm);
            this.Name = "ChildForm";
            this.Text = "ChildForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxChildForm;
    }
}