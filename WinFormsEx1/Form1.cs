﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsEx1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void button1_Click(object sender, EventArgs e)
        {
            ChildForm child = new ChildForm();
            child.Owner = this;
            child.ShowDialog();
        }

        public void textBoxMain_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
